const path = require('path');

module.exports = {
 
    entry: "./src/index.js",
    
    //tells webpack not to touch any built-in modules like "fs" || "path"
    target: 'node',

    output: {
        path: path.resolve(__dirname, "dist"),
        filename: 'index.js',
        libraryTarget: 'commonjs2'
    },

    experiments: {
        topLevelAwait: true
    }
};
