data "aws_region" "current" {}

data "archive_file" "zip_creator" {
    type = "zip"
    source_file = "../dist/index.js"
    output_path = "../archive/MonitorDLQDaily.zip"
}

data "aws_sqs_queue" "dlq_queue" {
    name = var.dlq_name
}

# log group, where the logs are comming from lambda function resource
resource "aws_cloudwatch_log_group" "log_group" {
    name = "/aws/lambda/${var.function_name}-${var.service_name}"
    retention_in_days = 7
    tags = merge(local.tags, var.tags)
}

resource "aws_lambda_function" "lambda_function" {
    function_name = "${var.function_name}-${var.service_name}"
    role = aws_iam_role.iam_lambda_execution_role.arn
 
    description = "Lambda function will periodically monitor Dlq on daily basis to check whether their are messages in queue, if so then it will raise an alert in slack channel"
    handler =  "index.handler"
    filename = data.archive_file.zip_creator.output_path
    publish = true
    runtime = "nodejs14.x"
    source_code_hash = "${data.archive_file.zip_creator.output_base64sha256}"
    
    environment {
      variables = local.env_map
    }

    depends_on = [
        aws_iam_role_policy_attachment.lambda_attachments,
        aws_cloudwatch_log_group.log_group
    ]

    tags = merge(local.tags, var.tags)

}

