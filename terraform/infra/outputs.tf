output "lambda_arn" {
    description = "The ARN for the lambda function"
    value = aws_lambda_function.lambda_function.arn
}

output "qualified_lambda_arn" {
    description = "The ARN for the current version of the lambda function"
    value = aws_lambda_function.lambda_function.qualified_arn
}

output "execution_role_arn" {
    description = "The ARN for the lambda function's IAM Role"
    value = aws_iam_role.iam_lambda_execution_role.arn
}