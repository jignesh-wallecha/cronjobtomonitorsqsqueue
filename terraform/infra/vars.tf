variable "function_name" {
    type = string 
    default = "MonitorDLQDaily"
}

variable "tags" {
    description = "map of tags to assign to lambda resource"
    type = map(string)
    default = {}
}

variable "env" {
    description = "service environment"
    type = string
}


variable "cron_expression" {
    type = string 
    description = "default cron expression is set for daily"
    default = "cron(30 6 * * ? *)"
}

variable "slack_webhook_url" {
    type = string
    description = "slack webhook url, and default is set for webhook of merchant-dev-alerts channel" 
    default = ""
}

variable "region" {
    type = string 
    description = "aws region to be set in environment variables of lambda function, default is set to eu-west-1"
    default = "eu-west-1"
}

variable "service_name" {
    type = string
    description = "the service name for this infra is used"
}

variable "dlq_name" {
    type = string 
    description = "the name of the dlq name"
}
