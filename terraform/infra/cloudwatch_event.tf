# Amazon Cloudwatch Events, which triggers the lambda function daily to monitor DLQ queue 
resource "aws_cloudwatch_event_rule" "trigger_lambda_on_event" {
    name = "${var.function_name}-${var.service_name}-cron"
    description = "AWS Cloudwatch events, triggers lambda function daily to monitor the DLQ resource"
    schedule_expression = var.cron_expression
    tags = merge(local.tags, var.tags)
}

#set target of trigger_lambda_on_event to lamba function (MonitorDlqDaily)
resource "aws_cloudwatch_event_target" "target_lambda_on_event" {
    target_id = "scheduled-job"
    rule = aws_cloudwatch_event_rule.trigger_lambda_on_event.name
    arn = aws_lambda_function.lambda_function.arn
}

# give permission to cloudwatch event to trigger lambda function
resource "aws_lambda_permission" "allow_cloudwatch_event" {
    statement_id = "MonitorDLQScheduledEvent"
    action = "lambda:InvokeFunction"
    function_name = aws_lambda_function.lambda_function.function_name
    principal = "events.amazonaws.com"
    source_arn = aws_cloudwatch_event_rule.trigger_lambda_on_event.arn
}