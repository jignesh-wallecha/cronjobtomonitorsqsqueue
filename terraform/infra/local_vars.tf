locals {  
    environment = var.env  
    env_map = {
        ENV = local.environment
        REGION = var.region
        DLQ_URL = data.aws_sqs_queue.dlq_queue.url
        SLACK_WEBHOOK_URL = var.slack_webhook_url
    }
    tags = {
        service = var.service_name
        environment = local.environment
        business_unit = "Cimpress"
        tribe = "Product Catalog Tribe"
    }
    iam_policy_arn = tolist([
        aws_iam_policy.lambda_log_policy.arn, 
        aws_iam_policy.dlq_access_policy.arn, 
        aws_iam_policy.kms_access_policy.arn
    ])
}