
# IAM LAMBDA EXECUTION ROLE
resource "aws_iam_role" "iam_lambda_execution_role" {
    name = "Lambda-${var.function_name}-Execution-Role"
    assume_role_policy = jsonencode({
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": "sts:AssumeRole",
                "Principal": {
                    "Service": "lambda.amazonaws.com"
                },
                "Effect": "Allow"
            }
        ]
    })
    tags = merge(local.tags, var.tags)
}

# policy allowing role write action to cloudwatch logs
resource "aws_iam_policy" "lambda_log_policy" {
    name = "Lambda-${var.function_name}-Execution-Policy"
    description = "this policy is for logging"
    policy = jsonencode({
        "Version" : "2012-10-17",
        "Statement": [
            {
                "Sid": "AllowWriteToCloudwatchLogs",
                "Effect": "Allow",
                "Action": [
                    "logs:PutLogEvents",
                    "logs:CreateLogStream"
                ],
                "Resource": "${aws_cloudwatch_log_group.log_group.arn}"
            }
        ]
    })
    tags = merge(local.tags, var.tags)
}

#policy allowing role to access DLQ
resource "aws_iam_policy" "dlq_access_policy" {
    name = "Lambda-${var.function_name}-DLQ-Access-Policy"
    description = "this access policy allows the lambda function to access SQS"
    policy = jsonencode({
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "sqs:GetQueueAttributes"
                ],
                "Resource": "${data.aws_sqs_queue.dlq_queue.arn}"
            }
        ]
    })
    tags = merge(local.tags, var.tags)
}

# policy allowing role to access KMS resource
resource "aws_iam_policy" "kms_access_policy" {
    name = "Lambda-${var.function_name}-KMS-Access-Policy"
    description = "this access policy allows the lambda function to access KMS"
    policy = jsonencode({
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": "kms:*",
                "Resource": "*"
            }
        ]
    })
    tags = merge(local.tags, var.tags)
}


# attach all policies to iam_execution_role
resource "aws_iam_role_policy_attachment" "lambda_attachments" {
    role = aws_iam_role.iam_lambda_execution_role.name
    count = length(local.iam_policy_arn)
    policy_arn = local.iam_policy_arn[count.index]
}
