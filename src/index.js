const { SQSClient, GetQueueAttributesCommand  } = require('@aws-sdk/client-sqs');
const axios = require('axios');

const SLACK_WEBHOOK_ENDPOINT = process.env.SLACK_WEBHOOK_URL;
const DLQ_URL = process.env.DLQ_URL;
const REGION = process.env.REGION;

const getNumOfMessages = async (dlqUrl) => {
    
    var client = new SQSClient({ region : REGION });
    try {
        const res = await client.send(new GetQueueAttributesCommand({
            QueueUrl: dlqUrl,
            AttributeNames: [
                "ApproximateNumberOfMessages",
                "QueueArn"
            ]
        }));
        return res.Attributes;
    } catch (err) {
        throw err;
    }
}

const notify = async (messageAttributes) => {

    var response = await axios({
        method: 'post',
        url: SLACK_WEBHOOK_ENDPOINT,
        data: JSON.stringify({
            blocks: [
                {
                    type: "section",
                    text: {
                        type: "mrkdwn",
                        text: `*Alert*: ${messageAttributes.ApproximateNumberOfMessages} *messages in Dead Letter Queue Arn* : ${messageAttributes.QueueArn}`
                    }
                }
            ]
        }),
        headers: {
            "Content-type": "application/json; charset=utf-8"
        }
    });

    if (response.status != 200) {
        throw new Error(response.statusText);
    }
    return response.data;
}


export const handler = async (event) => {
    try {
        const messageAttributes = await getNumOfMessages(DLQ_URL);
        const response = await notify(messageAttributes);
        return response;
    } catch (err) {
        throw err;
    }
}
